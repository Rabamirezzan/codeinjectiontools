package com.codeinjection.annotations.tool;

import java.lang.String; /**
 * Created by rramirezb on 16/01/2015.
 */
public @interface DefaultClassInjectionParams {
    String classPrefix() default "";
    String name() default "";
    String classSuffix() default "_";
    String classPackage() default "";
}
